<?php

namespace App\Services\Product;

use App\Http\Requests\Product\ProductRequest;

class ProductOption
{
    private string $name;
    private float $price;
    private array $categories;
    private bool $isActive;

    public function __construct(ProductRequest $request)
    {
        $this->name = $request->name;
        $this->price = $request->price;
        $this->categories = $request->categories;
        $this->isActive = $request->is_active;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getCategories(): array
    {
        return $this->categories;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function toArray(): array
    {
        return [
           'name' => $this->name,
           'price' => $this->price,
           'is_active' => $this->isActive,
        ];
    }
}
