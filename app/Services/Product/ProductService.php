<?php

namespace App\Services\Product;

use App\Exceptions\DomainException\LogicException;
use App\Exceptions\DomainException\NotFoundException;
use App\Models\Product;
use App\Repositories\Product\ProductRepository;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class ProductService
{
    private ProductRepository $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @throws LogicException
     */
    public function create(ProductOption $option)
    {
        $model = new Product();
        try {
            DB::transaction(function () use ($model, $option) {
                $model->name = $option->getName();
                $model->price = $option->getPrice();
                $model->is_active = $option->isActive();
                $model->save();
                $model->categories()->attach($option->getCategories());
            });
            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            throw new LogicException();
        }
    }

    /**
     * @throws NotFoundException
     * @throws LogicException
     */
    public function update(ProductOption $option, int $id)
    {
        $model = $this->repository->getProduct($id);
        try {
            DB::transaction(function () use ($model, $option) {
                $model->update($option->toArray());
                $model->categories()->sync($option->getCategories());
            });
            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            throw new LogicException();
        }

    }

    /**
     * @throws NotFoundException
     */
    public function destroy(int $id)
    {
        $model = $this->repository->getProduct($id);
        $model->delete();
    }
}
