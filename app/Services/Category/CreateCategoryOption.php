<?php

namespace App\Services\Category;

use App\Http\Requests\Category\CreateCategoryRequest;

class CreateCategoryOption
{
    private string $name;

    public function __construct(CreateCategoryRequest $request)
    {
        $this->name = $request->name;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
