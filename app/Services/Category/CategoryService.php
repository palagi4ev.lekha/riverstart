<?php

namespace App\Services\Category;

use App\Exceptions\DomainException\LogicException;
use App\Exceptions\DomainException\NotFoundException;
use App\Models\Category;
use App\Repositories\Category\CategoryRepository;
use Illuminate\Database\QueryException;

class CategoryService
{
    private CategoryRepository $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @throws LogicException
     */
    public function create(CreateCategoryOption $option)
    {
        $category = new Category();
        $category->name = $option->getName();
        if (!$category->save()) {
            throw new LogicException();
        }
    }

    /**
     * @throws NotFoundException
     * @throws LogicException
     */
    public function destroy(int $id)
    {
        $model = $this->repository->getCategory($id);
        try {
            $model->delete();
        } catch (QueryException $e) {
            throw new LogicException("В данной категории имеются продукты");
        }
    }
}
