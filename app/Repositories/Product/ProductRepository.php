<?php

namespace App\Repositories\Product;

use App\Exceptions\DomainException\NotFoundException;
use App\Models\Product;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class ProductRepository
{
    public function countProducts(ListOption $option): int
    {
        return $this->query($option)
            ->count();
    }

    public function getProducts(ListOption $option): Collection
    {
        return $this->query($option)
            ->with('categories')
            ->limit($option->getLimit())
            ->offset($option->getOffset())
            ->get();
    }

    /**
     * @throws NotFoundException
     * @noinspection PhpIncompatibleReturnTypeInspection
     */
    public function getProduct(int $id): Product
    {
        $model = Product::query()
            ->where(['id' => $id])
            ->first();

        if ($model) {
            return $model;
        }
        throw new NotFoundException();
    }

    private function query(ListOption $option): Builder
    {
        $query = Product::query()
            ->select('products.*');
        if ($option->getProduct()) {
            $query->where('name', 'like', '%' . $option->getProduct() . '%');
        }
        if ($option->getCategory()) {
            $query->join('product_category as pc', 'pc.id_product', '=', 'products.id')
                ->join('categories as c', 'c.id', '=', 'pc.id_category')
                ->where('c.name', 'like', '%' . $option->getCategory() . '%');
        }
        if ($option->getPriceFrom()) {
            $query->where('price', '<=', $option->getPriceFrom());
        }
        if ($option->getPriceBefore()) {
            $query->where('price', '>=', $option->getPriceBefore());
        }
        if (!is_null($option->getIsActive())) {
            $query->where(['is_active' => $option->getIsActive()]);
        }
        return $query;
    }
}
