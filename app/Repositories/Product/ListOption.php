<?php

namespace App\Repositories\Product;

use App\Http\Requests\Product\IndexProductRequest;

class ListOption
{
    private int $limit;
    private int $offset;
    private ?string $product;
    private ?string $category;
    private ?bool $isActive;
    private ?int $priceFrom;
    private ?int $priceBefore;

    public const LIMIT = 10;
    public const OFFSET = 0;

    public function __construct(IndexProductRequest $request)
    {
        $this->offset = $request->offset ?: self::OFFSET;
        $this->limit = $request->limit ?: self::LIMIT;
        $this->product = $request->product;
        $this->category = $request->category;
        $this->priceFrom = $request->price_from;
        $this->priceBefore = $request->price_before;
        $this->isActive = $request->is_active;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    public function getProduct(): ?string
    {
        return $this->product;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function getPriceFrom(): ?int
    {
        return $this->priceFrom;
    }

    public function getPriceBefore(): ?int
    {
        return $this->priceBefore;
    }
}
