<?php

namespace App\Repositories\Category;

use App\Exceptions\DomainException\NotFoundException;
use App\Models\Category;

class CategoryRepository
{
    /**
     * @throws NotFoundException
     * @noinspection PhpIncompatibleReturnTypeInspection
     */
    public function getCategory(int $id): Category
    {
        $model = Category::query()
            ->where(['id' => $id])
            ->first();

        if ($model) {
            return $model;
        }
        throw new NotFoundException();
    }
}
