<?php

namespace App\Exceptions\DomainException;

use Symfony\Component\HttpFoundation\Response;
use Throwable;

class NotFoundException extends DomainException
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct(
            $message ?: Response::$statusTexts[Response::HTTP_NOT_FOUND],
            $code ?: Response::HTTP_NOT_FOUND,
            $previous
        );
    }
}
