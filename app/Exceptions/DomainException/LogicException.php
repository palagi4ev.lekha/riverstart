<?php

namespace App\Exceptions\DomainException;

use Symfony\Component\HttpFoundation\Response;
use Throwable;

class LogicException extends DomainException
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct(
            $message ?: Response::$statusTexts[Response::HTTP_BAD_REQUEST],
            $code ?: Response::HTTP_BAD_REQUEST,
            $previous
        );
    }
}
