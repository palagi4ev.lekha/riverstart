<?php

namespace App\Models;

use Carbon\Carbon;
use Cknow\Money\Casts\MoneyIntegerCast;
use Cknow\Money\Money;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $name
 * @property Money $price
 * @property bool $is_active
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $deleted_at
 *
 * @property Collection|Category[] $categories
 *
 * @package App\Models
 */
class Product extends Model
{
    use SoftDeletes, HasFactory;

    protected $table = 'products';

    protected $casts = [
        'price' => MoneyIntegerCast::class,
        'is_active' => 'bool',
    ];

    protected $fillable = [
        'name',
        'price',
        'is_active',
    ];

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(
            Category::class,
            'product_category',
            'id_product',
            'id_category'
        );
    }
}
