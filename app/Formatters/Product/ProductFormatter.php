<?php

namespace App\Formatters\Product;

use App\Models\Category;
use App\Models\Product;
use App\Repositories\Product\ListOption;
use App\Repositories\Product\ProductRepository;
use Illuminate\Database\Eloquent\Collection;

class ProductFormatter
{
    private ProductRepository $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function format(ListOption $option): array
    {
        return [
            'count' => $this->repository->countProducts($option),
            'offset' => $option->getOffset(),
            'limit' => $option->getLimit(),
            'items' => $this->prepareProducts($this->repository->getProducts($option)),
        ];
    }

    private function prepareProducts(Collection $collection): array
    {
        return $collection->map(function (Product $model) {
            return [
                'id' => $model->id,
                'name' => $model->name,
                'price' => $model->price->formatByDecimal(),
                'categories' => $this->prepareCategories($model->categories),
            ];
        })->toArray();
    }

    private function prepareCategories(Collection $collection): array
    {
        return $collection->map(function (Category $category) {
            return [
                'id' => $category->id,
                'name' => $category->name,
            ];
        })->toArray();
    }
}
