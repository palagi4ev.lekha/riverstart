<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property ?int $offset
 * @property ?int $limit
 * @property ?string $product
 * @property ?string $category
 * @property ?float $price_from
 * @property ?float $price_before
 * @property ?bool $is_active
 */
class IndexProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'offset' => 'nullable|integer',
            'limit' => 'nullable|integer',
            'product' => 'nullable|string',
            'category' => 'nullable|string',
            'price_from' => 'nullable|float',
            'price_before' => 'nullable|float',
            'is_active' => 'nullable|boolean',
        ];
    }
}
