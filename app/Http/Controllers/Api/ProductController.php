<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\DomainException\LogicException;
use App\Exceptions\DomainException\NotFoundException;
use App\Formatters\Product\ProductFormatter;
use App\Http\Controllers\Controller;
use App\Http\Requests\Product\ProductRequest;
use App\Http\Requests\Product\IndexProductRequest;
use App\Repositories\Product\ListOption;
use App\Services\Product\ProductOption;
use App\Services\Product\ProductService;
use Illuminate\Http\JsonResponse;

class ProductController extends Controller
{
    public function index(ProductFormatter $formatter, IndexProductRequest $request): JsonResponse
    {
        return response()->json($formatter->format(new ListOption($request)));
    }

    /**
     * @throws LogicException
     */
    public function create(ProductService $service, ProductRequest $request): JsonResponse
    {
        $service->create(new ProductOption($request));
        return response()->json(['success' => true]);
    }

    /**
     * @throws NotFoundException
     * @throws LogicException
     */
    public function update(ProductService $service, ProductRequest $request, int $id): JsonResponse
    {
        $service->update(new ProductOption($request), $id);
        return response()->json(['success' => true]);
    }

    /**
     * @throws NotFoundException
     */
    public function destroy(ProductService $service, int $id): JsonResponse
    {
        $service->destroy($id);
        return response()->json(['success' => true]);
    }
}
