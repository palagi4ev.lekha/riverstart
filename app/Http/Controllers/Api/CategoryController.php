<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\DomainException\LogicException;
use App\Exceptions\DomainException\NotFoundException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Category\CreateCategoryRequest;
use App\Services\Category\CategoryService;
use App\Services\Category\CreateCategoryOption;
use Illuminate\Http\JsonResponse;

class CategoryController extends Controller
{
    /**
     * @throws LogicException
     */
    public function create(CategoryService $service, CreateCategoryRequest $request): JsonResponse
    {
        $service->create(new CreateCategoryOption($request));
        return response()->json(['success' => true]);
    }

    /**
     * @throws NotFoundException|LogicException
     */
    public function destroy(CategoryService $service, int $id): JsonResponse
    {
        $service->destroy($id);
        return response()->json(['success' => true]);
    }
}
