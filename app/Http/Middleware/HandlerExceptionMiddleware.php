<?php

namespace App\Http\Middleware;

use App\Exceptions\DomainException\DomainException;
use Closure;
use Illuminate\Http\Request;

class HandlerExceptionMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);

        if (!empty($response->exception) && $response->exception instanceof DomainException) {
            return response()->json([
                'success' => false,
                'message' => $response->exception->getMessage()
            ], $response->exception->getCode());
        }

        return $response;
    }
}
