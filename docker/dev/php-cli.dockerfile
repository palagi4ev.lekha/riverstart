FROM php:7.4-cli-alpine

ENV PHP_XDEBUG_VERSION 3.0.4

ENV COMPOSER_ALLOW_SUPERUSER 1

ENV PHPIZE_DEPS \
    autoconf cmake file g++ gcc libc-dev pcre-dev make git \
    pkgconf re2c oniguruma-dev freetype-dev libwebp-dev  \
    libpng-dev libjpeg-turbo-dev libxslt-dev

RUN apk add --no-cache --virtual .persistent-deps \
    icu-dev postgresql-dev libxml2-dev freetype \
    libpng libjpeg-turbo bzip2-dev libintl gettext-dev libxslt

RUN apk add --no-cache --virtual libtool librsvg ttf-dejavu

RUN set -xe \
    && ln -s /usr/lib /usr/local/lib64 \
    && apk add --no-cache --virtual .build-deps $PHPIZE_DEPS \
    && docker-php-ext-configure gd --enable-gd --with-freetype --with-jpeg --with-webp \
    && docker-php-ext-configure bcmath --enable-bcmath \
    && docker-php-ext-configure intl --enable-intl \
    && docker-php-ext-configure pcntl --enable-pcntl \
    && docker-php-ext-configure mysqli --with-mysqli \
    && docker-php-ext-configure pdo_mysql --with-pdo-mysql \
    && docker-php-ext-configure mbstring --enable-mbstring \
    && docker-php-ext-configure soap --enable-soap \
    && docker-php-ext-install -j$(nproc) \
        bcmath intl pcntl \
        mysqli pdo_mysql \
        mbstring soap bz2 \
        calendar exif gettext \
        shmop sockets sysvmsg \
        sysvsem sysvshm xsl \
    && rm -rf /tmp/*

RUN apk add --no-cache libzip-dev zip \
    && docker-php-ext-configure zip \
    && docker-php-ext-install zip

RUN apk add --no-cache $PHPIZE_DEPS \
    && pecl install xdebug-${PHP_XDEBUG_VERSION} \
    && docker-php-ext-enable xdebug

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/bin --filename=composer --quiet

ARG GID
ARG UID
ARG GROUP_NAME
ARG USER_NAME

RUN addgroup -g $GID $GROUP_NAME && \
    adduser -u $UID -G $GROUP_NAME -h /home/$USER_NAME -D $USER_NAME

USER $USER_NAME

WORKDIR /app