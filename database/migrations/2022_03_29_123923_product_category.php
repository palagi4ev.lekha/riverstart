<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_category', function (Blueprint $table) {
            $table->bigInteger('id_product')->unsigned()->index();
            $table->foreign('id_product')->references('id')->on('products');
            $table->bigInteger('id_category')->unsigned()->index();
            $table->foreign('id_category')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_category', function (Blueprint $table) {
            $table->dropForeign(['id_product']);
            $table->dropForeign(['id_category']);
            $table->dropIndex(['id_product']);
            $table->dropIndex(['id_category']);
            $table->drop();
        });
    }
}
