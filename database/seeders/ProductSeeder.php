<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = now();
        $id = DB::table('products')->insertGetId([
            'name' => 'Молоко',
            'price' => 100,
            'is_active' => 1,
            'created_at' => $now,
        ]);
        $product = DB::table('categories')->where(['name' => 'Продукты'])->first();
        $idCategory = DB::table('categories')->where(['name' => 'Молочные'])->first();
        $this->insertCategory($id, $product->id);
        $this->insertCategory($id, $idCategory->id);
        $idCategory = DB::table('categories')->where(['name' => 'Кисломолочные'])->first();
        $id = DB::table('products')->insertGetId([
            'name' => 'Кефир',
            'price' => 200,
            'is_active' => 1,
            'created_at' => $now,
        ]);
        $this->insertCategory($id, $product->id);
        $this->insertCategory($id, $idCategory->id);
        $id = DB::table('products')->insertGetId([
            'name' => 'Ряженка',
            'price' => 200,
            'is_active' => 0,
            'created_at' => $now,
        ]);
        $this->insertCategory($id, $product->id);
        $this->insertCategory($id, $idCategory->id);
    }

    private function insertCategory(int $idProduct, int $idCategory)
    {
        DB::table('product_category')->insert([
            'id_product' => $idProduct,
            'id_category' => $idCategory,
        ]);
    }
}
