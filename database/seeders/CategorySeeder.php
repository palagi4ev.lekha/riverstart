<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = now();
        DB::table('categories')->insert(
            [
                [
                    'name' => 'Продукты',
                    'created_at' => $now,
                ],
                [
                    'name' => 'Молочные',
                    'created_at' => $now,
                ],
                [
                    'name' => 'Кисломолочные',
                    'created_at' => $now,
                ],
            ]);
    }
}
